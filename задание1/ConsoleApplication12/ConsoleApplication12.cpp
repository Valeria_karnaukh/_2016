// ConsoleApplication12.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include <iostream>
#include <math.h>
using namespace std;

double function_name(double z) {
	double result = 0;
	if(z < 0)
		result = pow(z, 2) + 2 * sin(z);
	else if (0 <= z && z <= 8)
		result = log(z) + 2 * z;
	else (z > 8);
        result = exp(z) + 1 / z;
	double y = pow(cos(result), 3);
		cout << "y = " << y << endl;
	return y;
}

int _tmain(int argc, _TCHAR* argv[])
{
	double z, y;
	cout << "Enter random figure: ";
	cin >> z;
	y=function_name(z);
	system("pause");
	return 0;
}

