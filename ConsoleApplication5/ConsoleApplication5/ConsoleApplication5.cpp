// ConsoleApplication5.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

double A(double &x)
{
	return cos(x)+2*x;
}

double B(double x)
{
	return x*4-((2*x)/5);
}

double C(double x)
{
	return 2*x-5;
}

double H(double a, double b, double c)
{
	return (4 * pow(a, 2)) + (5 * pow(b, 2));
}

int main()
{
	cout << "Enter a number: ";
	double x;
	cin >> x;

	double a = A(x);
	double b = B(x);
	double c = C(x);

	double h = H(a, b, c);

	cout << "h() = " << h;

	cin.ignore();
	cin.get();
	return 0;
}